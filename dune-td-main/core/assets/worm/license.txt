Model Information:
* title:	FNAF 2 old Foxy
* source:	https://sketchfab.com/3d-models/fnaf-2-old-foxy-f274c3b948ad4663bb6abf437ef8cfd0
* author:	Wrenge (https://sketchfab.com/wrenge)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "FNAF 2 old Foxy" (https://sketchfab.com/3d-models/fnaf-2-old-foxy-f274c3b948ad4663bb6abf437ef8cfd0) by Wrenge (https://sketchfab.com/wrenge) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)