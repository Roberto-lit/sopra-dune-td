package io.swapastack.tests;

import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.utils.Queue;
import io.swapastack.dunetd.*;
import net.mgsx.gltf.scene3d.scene.Scene;
import org.junit.Assert;
import org.junit.Test;


public class GameScreenTest4 {

    DuneTD td = new DuneTD();
    GameScreen gs = new GameScreen(td);

    @Test
    public void testReduceHealth(){
        GameScreen.setHealth(GameScreen.getHealth());
        Assert.assertEquals(0, GameScreen.getHealth());
    }


    @Test
    public void testPathExists(){
        GraphPath<Tile> graphPath = gs.getGraphPath(gs.getFealdFree());
        Assert.assertTrue(graphPath.getCount() != 0);
    }

    @Test
    public void testNoPathExists(){
        boolean[][] blockPortals = gs.getFealdFree();

        blockPortals[0][1] = true;
        blockPortals[1][0] = true;
        blockPortals[gs.getCols()-1][gs.getRows()-2] = true;
        blockPortals[gs.getCols()-2][gs.getRows()-1] = true;

        GraphPath<Tile> graphPath = gs.getGraphPath(blockPortals);

        Assert.assertEquals(0, graphPath.getCount());
    }

    @Test
    public void pathIsValidPath(){
        GraphPath<Tile> graphPath = gs.getGraphPath(gs.getFealdFree());
        Queue<Tile> tileQueue = new Queue<>();
        for (Tile tile : graphPath) {
            tileQueue.addLast(tile);
        }
        Tile current = tileQueue.removeFirst();
        while (tileQueue.notEmpty()){
            Tile next = tileQueue.first();
            Assert.assertTrue((current.getX() != next.getX() && current.getZ() == next.getZ()) || (current.getX() == next.getX() && current.getZ() != next.getZ()));
            current = tileQueue.removeFirst();
        }
    }

    @Test
    public void testBulletDoesDamage(){
        TowerUnit towerTest = new ArtilleryTower(new Pair<>(0.0f, 1.0f), new Scene(new Model()));
        EnemyUnit enemyTest = new Infantry(new Scene(new Model()), gs.getGraphPath(gs.getFealdFree()), new Tile(0, 0));
        long initHealth = enemyTest.getHealth();
        Bullet bulletTest = new Bullet(new Pair<>(towerTest.getPosition().getFirst(), towerTest.getPosition().getSecond()), new Scene(new Model()), towerTest, enemyTest);
        do{
            bulletTest.update();
        }while (bulletTest.getScalar() <= 1);
        bulletTest.update();
        Assert.assertTrue(initHealth != enemyTest.getHealth());
    }

    @Test
    public void testPairIsEqual(){
        Pair<Float, Float> pair1 = new Pair<>(1.0f, 1.0f);
        Pair<Float, Float> pair2 = new Pair<>(1.0f, 1.0f);

        Assert.assertTrue(pair1.equals(pair2));
    }

    @Test
    public void testPairIsDifferent(){
        Pair<Float, Float> pair1 = new Pair<>(1.0f, 1.0f);
        Pair<Float, Float> pair2 = new Pair<>(0.0f, 0.0f);

        Assert.assertFalse(pair1.equals(pair2));
    }

    @Test
    public void testWorm(){
        Thumper thumper1 = new Thumper(new Pair<>(0.0f, 0.0f), new Scene(new Model()));
        Thumper thumper2 = new Thumper(new Pair<>(0.0f, (float) gs.getRows()), new Scene(new Model()));
        Thumper thumper3 = new Thumper(new Pair<>((float) gs.getCols(), 0.0f), new Scene(new Model()));
        Worm testWorm1 = new Worm(new Scene(new Model()), thumper1, thumper2, gs.getRows());
        Worm testWorm2 = new Worm(new Scene(new Model()), thumper1, thumper3, gs.getRows());

        Pair<Float, Float> posWorm1 = new Pair<>(testWorm1.getPosition().getFirst(), testWorm1.getPosition().getSecond());
        Pair<Float, Float> posWorm2 = new Pair<>(testWorm2.getPosition().getFirst(), testWorm2.getPosition().getSecond());

        while (testWorm1.getScalar() <= testWorm1.getMaxPosition()){
            testWorm1.start();
        }
        testWorm1.start();

        while (testWorm2.getScalar() <= testWorm2.getMaxPosition()){
            testWorm2.start();
        }
        testWorm2.start();

        Assert.assertTrue(!(posWorm1.equals(testWorm1.getPosition())) && testWorm1.isReached());
        Assert.assertTrue(!(posWorm2.equals(testWorm2.getPosition())) && testWorm2.isReached());
    }
}