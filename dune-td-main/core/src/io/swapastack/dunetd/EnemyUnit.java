package io.swapastack.dunetd;

import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Queue;
import net.mgsx.gltf.scene3d.scene.Scene;
import org.lwjgl.system.CallbackI;

public abstract class EnemyUnit {
    protected long health;
    protected float velocity;
    protected boolean grounded;
    protected Pair<Float,Float> position;
    protected Scene scene;
    protected long damage;
    protected boolean reached = false;
    protected boolean isSlowed;

    protected Tile nextTile;
    protected Queue<Tile> pathQueue = new Queue<>();
    protected GraphPath<Tile> graphPath;

    public EnemyUnit(Scene scene, GraphPath<Tile> graphPath, Tile start){
        this.graphPath = graphPath;
        this.nextTile = start;
        this.position = new Pair<>(start.x, start.z);
        this.scene = scene;
        fillQueue();
    }

    /**
     * Fills the Queue with all Tiles from the graphPath in start to goal order
     */
    private void fillQueue(){
        for (Tile tile : graphPath  ) {
            pathQueue.addLast(tile);
        }
    }

    public long getHealth() {
        return health;
    }

    public float getVelocity() {
        return velocity;
    }

    public Pair<Float, Float> getPosition() {
        return position;
    }

    public boolean isGrounded() {
        return grounded;
    }

    public GraphPath<Tile> getGraphPath() {
        return graphPath;
    }

    public Scene getScene() {
        return scene;
    }

    public long getDamage() {
        return damage;
    }

    public void setReached(boolean reached) {
        this.reached = reached;
    }

    /**
     * Methode for updating position of enemy
     */
    public abstract void step();

    /**
     * Gets called in step()
     * Checks if the Enemy already reached the next tile
     */
    void checkCollision(){
        if (pathQueue.size > 0) {
            Tile targetTile = pathQueue.first();
            int tempX = (int) (position.getFirst() * 10);
            int tempZ = (int) (position.getSecond() * 10);
            int nextX = (int) (targetTile.x * 10);
            int nextZ = (int) (targetTile.z * 10);
            tempX = tempX / 10;
            tempZ = tempZ / 10;
            nextX = nextX / 10;
            nextZ = nextZ / 10;
            if (Vector2.dst(tempX, tempZ, nextX, nextZ) < 0.01f) {
                reachNextTile();
            }
        }
    }

    /**
     * Gets called in checkCollision() if the nextTile is reached
     * Updates the current tile to the next tile and the next tile of the new current one.
     * Checks if the next tile is the goalTile
     */
    private void reachNextTile() {
        Tile nTile = pathQueue.first();

        this.nextTile = nTile;
        pathQueue.removeFirst();
        if (pathQueue.size != 0) nextTile = pathQueue.first();
        this.position.setFirst(nTile.x);
        this.position.setSecond(nTile.z);

        if (pathQueue.size == 0)reachDestination();
    }

    /**
     * Gets called in reachNextTile() if the enemy reached the end portal
     * Damages the player with the damage of the enemy
     */
    private void reachDestination(){
        reached = true;
        GameScreen.setHealth(damage);
    }




}
