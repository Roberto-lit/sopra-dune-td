package io.swapastack.dunetd;

import net.mgsx.gltf.scene3d.scene.Scene;

public class Worm {

    private Scene scene;
    private Thumper thumper1;
    private Thumper thumper2;
    private float scalar = 0;
    private int maxPosition;
    private boolean reached = false;
    private Pair<Float, Float> position;

    public Worm(Scene scene, Thumper thumper1, Thumper thumper2, int maxPosition){
        this.scene = scene;
        this.thumper1 = thumper1;
        this.thumper2 = thumper2;
        this.maxPosition = maxPosition;
        this.position = new Pair<>(0.0f, 0.0f);
    }

    /**
     * Gets called in destroy() in the GameScreen class when the worm was activated
     * Moves the worm model depending on the position of both thumpers
     */
    public void start(){
        if (thumper1.position.getFirst().equals(thumper2.position.getFirst())){
            if (scalar >= maxPosition){
                reached = true;
            }else {
                scene.modelInstance.transform.setToTranslation(thumper1.position.getFirst() , 0.2f, 0 + scalar).scale(0.02f, 0.02f, 0.02f);
                position.setFirst(thumper1.position.getFirst());
                position.setSecond(scalar);
                scalar += 0.1f;
            }
        }else if (thumper1.position.getSecond().equals(thumper2.position.getSecond())){
            if (scalar >= maxPosition){
                reached = true;
            }else {
                scene.modelInstance.transform.setToTranslation(0 + scalar , 0.2f, thumper1.position.getSecond()).scale(0.02f, 0.02f, 0.02f).rotate(0, 1, 0, 90);
                position.setSecond(thumper1.position.getSecond());
                position.setFirst(scalar);
                scalar += 0.1f;
            }
        }
    }

    public Pair<Float, Float> getPosition() {
        return position;
    }

    public Scene getScene() {
        return scene;
    }

    public boolean isReached() {
        return reached;
    }

    public float getScalar() {
        return scalar;
    }

    public int getMaxPosition() {
        return maxPosition;
    }
}
