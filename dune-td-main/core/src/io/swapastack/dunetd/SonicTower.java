package io.swapastack.dunetd;

import com.badlogic.gdx.math.Circle;
import net.mgsx.gltf.scene3d.scene.Scene;

public class SonicTower extends TowerUnit{
    public SonicTower(Pair<Float, Float> position, Scene scene) {
        super(position, scene);
        this.damage = 0;
        this.fireRate = 8.0f;
        this.range = 2.0f;
        this.price = 70;
        circle = new Circle(position.getFirst(), position.getSecond(), range);
    }
}
