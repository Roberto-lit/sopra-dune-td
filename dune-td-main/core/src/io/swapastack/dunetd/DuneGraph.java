package io.swapastack.dunetd;

import com.badlogic.gdx.ai.pfa.Connection;
import com.badlogic.gdx.ai.pfa.DefaultGraphPath;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.ai.pfa.indexed.IndexedAStarPathFinder;
import com.badlogic.gdx.ai.pfa.indexed.IndexedGraph;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

public class DuneGraph implements IndexedGraph<Tile> {
    DuneHeuristic duneHeuristic = new DuneHeuristic();
    Array<Tile> tiles = new Array<>();
    Array<Path> paths = new Array<>();

    ObjectMap<Tile, Array<Connection<Tile>>> pathMap = new ObjectMap<>();

    private int lastNodeIndex = 0;

    /**
     * Gets called in the GameScreen class in getGraphPath()
     * @param tile the Tile that gets added to the tiles list
     */
    public void addTile(Tile tile){
        tile.index = lastNodeIndex;
        lastNodeIndex++;
        tiles.add(tile);
    }

    /**
     * Gets called in the GameScreen class in getGraphPath()
     * the connection of one tile to an adjacent one
     * @param fromTile
     * @param toTile
     */
    public void connectTiles(Tile fromTile, Tile toTile){
        Path path = new Path(fromTile, toTile);
        if (!pathMap.containsKey(fromTile)){
            pathMap.put(fromTile, new Array<Connection<Tile>>());
        }
        pathMap.get(fromTile).add(path);
        paths.add(path);
    }

    /**
     * Gets called in the GameScreen class in getGraphPath()
     * @param startTile The tile where the A* algorithm should start
     * @param goalTile The tile where the A* algorithm needs to find the shortest path to
     * @return the Path in which the enemies need to walk to get to the goal the fastest way
     */
    public GraphPath<Tile> findPath(Tile startTile, Tile goalTile){
        GraphPath<Tile> tilePath = new DefaultGraphPath<>();
        new IndexedAStarPathFinder<>(this).searchNodePath(startTile, goalTile, duneHeuristic, tilePath);
        return tilePath;
    }


    @Override
    public int getIndex(Tile node) {
        return node.index;
    }

    @Override
    public int getNodeCount() {
        return lastNodeIndex;
    }

    @Override
    public Array<Connection<Tile>> getConnections(Tile fromNode) {
        if (pathMap.containsKey(fromNode)){
            return pathMap.get(fromNode);
        }
        return new Array<>(0);
    }
}
