package io.swapastack.dunetd;

import com.badlogic.gdx.ai.pfa.GraphPath;
import net.mgsx.gltf.scene3d.scene.Scene;

public class BossUnit extends EnemyUnit{
    public BossUnit(Scene scene, GraphPath<Tile> graphPath, Tile start) {
        super(scene, graphPath, start);
        this.health = 800;
        this.velocity = 0.04f;
        this.grounded = true;
        this.damage = 70;
    }

    /**
     * Gets called whenever a BossUnit is ine the enemyList in GameScreen
     * Increases the position of the model by the velocity to the next tile
     */
    public void step(){
        float tempX = position.getFirst();
        float tempZ = position.getSecond();

        //bewegung nach rechts
        if (Math.floor(tempX) == Math.floor(nextTile.x) && Math.floor(tempZ) == Math.floor(nextTile.z)-1) {
            scene.modelInstance.transform.setToTranslation(tempX, 0.25f, tempZ+velocity).scale(0.005f, 0.005f, 0.005f);
            position.setSecond(tempZ+velocity);
            //bewegung nach links
        } else if (Math.ceil(tempX) == Math.ceil(nextTile.x) && Math.ceil(tempZ) == Math.ceil(nextTile.z)+1) {
            scene.modelInstance.transform.setToTranslation(tempX, 0.25f, tempZ-velocity).scale(0.005f, 0.005f, 0.005f);
            position.setSecond(tempZ-velocity);
            //bewegung nach unten
        }else if (Math.floor(tempZ) == Math.floor(nextTile.z) && Math.floor(tempX) == Math.floor(nextTile.x)-1) {
            scene.modelInstance.transform.setToTranslation(tempX+velocity, 0.25f, tempZ).scale(0.005f, 0.005f, 0.005f);
            position.setFirst(tempX+velocity);
            //bewegung nach oben
        }else if (Math.ceil(tempZ) == Math.ceil(nextTile.z) && Math.ceil(tempX) == Math.ceil(nextTile.x)+1) {
            scene.modelInstance.transform.setToTranslation(tempX-velocity, 0.25f, tempZ).scale(0.005f, 0.005f, 0.005f);
            position.setFirst(tempX-velocity);
        }else scene.modelInstance.transform.setToTranslation(tempX, 0.25f, tempZ).scale(0.005f, 0.005f, 0.005f);
        checkCollision();
    }
}
