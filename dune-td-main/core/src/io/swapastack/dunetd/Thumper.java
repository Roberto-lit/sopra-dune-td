package io.swapastack.dunetd;

import com.badlogic.gdx.math.Circle;
import net.mgsx.gltf.scene3d.scene.Scene;

public class Thumper extends TowerUnit{
    public Thumper(Pair<Float, Float> position, Scene scene) {
        super(position, scene);
        this.damage = 0;
        this.fireRate = 0.0f;
        this.range = 1.0f;
        this.price = 500;
        circle = new Circle(position.getFirst(), position.getSecond(), range);
    }
}
