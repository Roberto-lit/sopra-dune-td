package io.swapastack.dunetd;

import com.badlogic.gdx.ai.pfa.Heuristic;
import com.badlogic.gdx.math.Vector2;

public class DuneHeuristic implements Heuristic<Tile> {

    /**
     * Is used in findPath() in the DuneGraph class
     * Is needed for calculating the path with a* algorithm
     * @param node the current tile
     * @param endNode the goalTile
     * @return the estimated distance between the current and the goalTile
     */
    @Override
    public float estimate(Tile node, Tile endNode) {
        return Vector2.dst(node.x, node.z, endNode.x, endNode.z );
    }
}
