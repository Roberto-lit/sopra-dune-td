package io.swapastack.dunetd;

import com.badlogic.gdx.math.Circle;
import net.mgsx.gltf.scene3d.scene.Scene;

public abstract class TowerUnit {
    protected Pair<Float, Float> position;
    protected Circle circle;
    protected long damage;
    protected float fireRate;
    protected float range;
    protected long price;
    protected Scene scene;
    protected boolean isShooting = false;

    public TowerUnit(Pair<Float, Float> position, Scene scene){
        this.position = position;
        this.scene = scene;
    }

    public Pair<Float, Float> getPosition() {
        return position;
    }

    public long getPrice(){
        return this.price;
    }

    public Scene getScene() {
        return scene;
    }

    public long getDamage() {
        return damage;
    }
}
