package io.swapastack.dunetd;

import com.badlogic.gdx.math.Circle;
import net.mgsx.gltf.scene3d.scene.Scene;

public class BombTower extends TowerUnit{

    public BombTower(Pair<Float, Float> position, Scene scene) {
        super(position, scene);
        this.damage = 40;
        this.fireRate = 3.0f;
        this.range = 2.0f;
        this.price = 100;
        circle = new Circle(position.getFirst(), position.getSecond(), range);
    }
}
