package io.swapastack.dunetd;


import com.badlogic.gdx.ai.pfa.Connection;

public class Path implements Connection<Tile> {

    Tile fromTile;
    Tile toTile;

    public Path(Tile fromTile, Tile toTile){
        this.fromTile = fromTile;
        this.toTile = toTile;
    }

    @Override
    public float getCost() {
        return 0;
    }

    @Override
    public Tile getFromNode() {
        return fromTile;
    }

    @Override
    public Tile getToNode() {
        return toTile;
    }
}
