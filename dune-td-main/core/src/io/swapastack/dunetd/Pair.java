package io.swapastack.dunetd;

import java.util.Objects;

public final class Pair<X extends Comparable<X>, Z extends Comparable<Z>> implements Comparable<Pair<X, Z>> {

    private X first;
    private Z second;

    public Pair(X first, Z second) {
        this.first = first;
        this.second = second;
    }

    public X getFirst() {
        return first;
    }

    public Z getSecond() {
        return second;
    }

    public void setFirst(X first){
        this.first = first;
    }

    public void setSecond(Z second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pair<?, ?> pair = (Pair<?, ?>) o;
        return Objects.equals(first, pair.first) &&
                Objects.equals(second, pair.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    public String toString() {
        return "Pair(X: " + this.first + ", Z: " + this.second + ")";
    }

    @Override
    public int compareTo(Pair<X, Z> o) {
        if (this.first.compareTo(o.first) != 0){
            return this.first.compareTo(o.first);
        }else return this.second.compareTo(o.second);
    }
}
