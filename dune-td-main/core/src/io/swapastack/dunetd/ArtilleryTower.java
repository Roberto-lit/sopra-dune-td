package io.swapastack.dunetd;

import com.badlogic.gdx.math.Circle;
import net.mgsx.gltf.scene3d.scene.Scene;

public class ArtilleryTower extends TowerUnit{

    public ArtilleryTower(Pair<Float, Float> position, Scene scene) {
        super(position, scene);
        this.damage = 5;
        this.fireRate = 10.0f;
        this.range = 3.0f;
        this.price = 50;
        circle = new Circle(position.getFirst(), position.getSecond(), range);
    }
}
