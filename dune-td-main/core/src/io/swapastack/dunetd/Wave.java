package io.swapastack.dunetd;

import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.assets.AssetManager;
import net.mgsx.gltf.scene3d.scene.Scene;
import net.mgsx.gltf.scene3d.scene.SceneAsset;

import java.util.ArrayList;
import java.util.HashMap;

public class Wave {

    private GraphPath<Tile> graphPath;
    private int waveCounter;
    private HashMap<String, SceneAsset> assetHashMap;

    public Wave(GraphPath<Tile> graphPath, int waveCounter, HashMap<String, SceneAsset> assetHashMap){
        this.graphPath = graphPath;
        this.waveCounter = waveCounter;
        this.assetHashMap = assetHashMap;
    }

    /**
     * Gets called in delayEnemies() in the GameScreen class
     * Generates a list of enemies depending on the waveCounter.
     * @return the generated list of enemies and their order
     */
    public ArrayList<EnemyUnit> getWave(){
        ArrayList<EnemyUnit> temp = new ArrayList<>();
        for (int i = 1; i <= waveCounter; i++) {
            temp.add(new Infantry(new Scene(assetHashMap.get("cute_cyborg/scene.gltf").scene), graphPath, new Tile(0, 0)));
            if (i % 5 == 0)temp.add(new Harvester(new Scene(assetHashMap.get("spaceship_orion/scene.gltf").scene), graphPath, new Tile(0, 0)));
            if (i % 7 == 0)temp.add(new BossUnit(new Scene(assetHashMap.get("faceted_character/scene.gltf").scene), graphPath, new Tile(0, 0)));
            if (i % 13 == 0){
                temp.add(new Infantry(new Scene(assetHashMap.get("cute_cyborg/scene.gltf").scene), graphPath, new Tile(0, 0)));
                temp.add(new Harvester(new Scene(assetHashMap.get("spaceship_orion/scene.gltf").scene), graphPath, new Tile(0, 0)));
            }
            if (i % 19 == 0){
                temp.add(new Infantry(new Scene(assetHashMap.get("cute_cyborg/scene.gltf").scene), graphPath, new Tile(0, 0)));
                temp.add(new BossUnit(new Scene(assetHashMap.get("faceted_character/scene.gltf").scene), graphPath, new Tile(0, 0)));
            }
            if (i % 23 == 0){
                temp.add(new Infantry(new Scene(assetHashMap.get("cute_cyborg/scene.gltf").scene), graphPath, new Tile(0, 0)));
                temp.add(new Harvester(new Scene(assetHashMap.get("spaceship_orion/scene.gltf").scene), graphPath, new Tile(0, 0)));
                temp.add(new BossUnit(new Scene(assetHashMap.get("faceted_character/scene.gltf").scene), graphPath, new Tile(0, 0)));
            }
            if (i % 27 == 0){
                for (int j = 0; j < 10; j++) {
                    temp.add(new BossUnit(new Scene(assetHashMap.get("faceted_character/scene.gltf").scene), graphPath, new Tile(0, 0)));
                }
            }
        }
        return temp;
    }
}
