package io.swapastack.dunetd;

public class Tile {
    float x;
    float z;

    int index;

    public Tile(float x, float z){
        this.x = x;
        this.z = z;
    }

    public void setIndex(int index){
        this.index = index;
    }

    public float getX() {
        return x;
    }

    public float getZ() {
        return z;
    }
}
