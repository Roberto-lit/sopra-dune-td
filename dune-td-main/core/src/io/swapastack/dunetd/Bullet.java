package io.swapastack.dunetd;

import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import net.mgsx.gltf.scene3d.scene.Scene;

public class Bullet {
    private Scene scene;
    private Pair<Float, Float> position;
    public boolean remove = false;
    private final TowerUnit fromTower;
    private final EnemyUnit toEnemy;
    private Vector2 direction;
    private float scalar = 0;
    protected boolean isBomb;

    public Bullet(Pair<Float, Float> position, Scene scene, TowerUnit fromTower, EnemyUnit toEnemy){
        this.position = position;
        this.fromTower = fromTower;
        this.toEnemy = toEnemy;
        this.scene = scene;
        getDirection();
    }

    /**
     * Gets called whenever a new object of Bullet gets initialized
     * Calculates the vector of the bullet
     */
    private void getDirection(){
        this.direction = new Vector2(fromTower.position.getFirst() - toEnemy.position.getFirst(), fromTower.position.getSecond() - toEnemy.position.getSecond());
    }

    public Scene getScene() {
        return scene;
    }

    /**
     * Gets called whenever there is a Bullet instance in the bulletList in GameScreen
     * Moves the position of the model from the tower position to the enemy position until the scalar is 1.
     */
    public void update(){
        if (scalar >= 1.0f){
            remove = true;
            doDamage();
        }else {
            scene.modelInstance.transform.setToTranslation(fromTower.position.getFirst() - scalar * direction.x, 0.5f, fromTower.position.getSecond() - scalar * direction.y).scale(0.005f,0.005f,0.005f);
            scalar += 0.075f;
            this.position.setFirst(fromTower.position.getFirst() - scalar * direction.x);
            this.position.setSecond(fromTower.position.getSecond() - scalar * direction.y);
        }
    }

    /**
     * Gets called in update()
     * Indicator if the GameScreen class needs to calculate the aoe damage. When not than the damage gets subtracted from the enemy health
     */
    private void doDamage(){
        if (fromTower instanceof BombTower) isBomb = true;
        else toEnemy.health -= fromTower.damage;
    }

    public float getScalar() {
        return scalar;
    }

    public TowerUnit getFromTower() {
        return fromTower;
    }

    public EnemyUnit getToEnemy() {
        return toEnemy;
    }

    public Pair<Float, Float> getPosition() {
        return position;
    }
}
