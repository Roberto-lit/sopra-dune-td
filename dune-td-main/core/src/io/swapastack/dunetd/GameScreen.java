package io.swapastack.dunetd;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.ai.pfa.GraphPath;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Graphics;
import com.badlogic.gdx.backends.lwjgl3.audio.Mp3;
import com.badlogic.gdx.backends.lwjgl3.audio.Wav;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.AnimationController;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Queue;
import imgui.ImGui;
import imgui.flag.ImGuiWindowFlags;
import imgui.gl3.ImGuiImplGl3;
import imgui.glfw.ImGuiImplGlfw;
import net.mgsx.gltf.scene3d.attributes.PBRCubemapAttribute;
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute;
import net.mgsx.gltf.scene3d.lights.DirectionalLightEx;
import net.mgsx.gltf.scene3d.scene.Scene;
import net.mgsx.gltf.scene3d.scene.SceneAsset;
import net.mgsx.gltf.scene3d.scene.SceneManager;
import net.mgsx.gltf.scene3d.scene.SceneSkybox;
import net.mgsx.gltf.scene3d.utils.IBLBuilder;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * The GameScreen class.
 *
 * @author Dennis Jehle
 */
public class GameScreen implements Screen {

    private final DuneTD parent;

    // GDX GLTF
    private SceneManager sceneManager;
    private Cubemap diffuseCubemap;
    private Cubemap environmentCubemap;
    private Cubemap specularCubemap;
    private Texture brdfLUT;
    private SceneSkybox skybox;
    private DirectionalLightEx light;

    // libGDX
    private PerspectiveCamera camera;
    private CameraInputController cameraInputController;

    // 3D models
    String basePath = "kenney_gltf/";
    String kenneyAssetsFile = "kenney_assets.txt";
    String[] kenneyModels;
    HashMap<String, SceneAsset> sceneAssetHashMap;

    // Grid Specifications
    private int rows = 5;
    private int cols = 5;

    // Animation Controllers
    AnimationController bossCharacterAnimationController;
    AnimationController enemyCharacterAnimationController;
    AnimationController spaceshipAnimationController;

    // SpaiR/imgui-java
    public ImGuiImplGlfw imGuiGlfw = new ImGuiImplGlfw();
    public ImGuiImplGl3 imGuiGl3 = new ImGuiImplGl3();
    long windowHandle;

    // For black grid
    private Vector3 modelDimensions = new Vector3();
    private Environment environment = new Environment();
    public ModelBatch modelBatch;
    public ModelBuilder modelBuilder;
    public Model grid;
    public ModelInstance gridInstance;
    public ArrayList<ModelInstance> modelList = new ArrayList<>();

    // For highlighting position of mouse on grid
    private Ray mouseRay;
    private Vector3 mouseVector = new Vector3();
    private ModelInstance coloredGridInstance;
    private Model coloredGrid;
    ArrayList<BoundingBox> boundingBoxList = new ArrayList<>();

    private static long health = 1000;
    private long spice = 100;
    private long score = 0;

    //For everything tower-related
    private int thumperCount = 0;
    private boolean boughtSonic = false;
    private boolean boughtArtillery = false;
    private boolean boughtBomb = false;
    private boolean boughtThumper = false;
    private Scene sonicTowerTemp;
    private Scene bombTowerTemp;
    private Scene artilleryTowerTemp;
    private Scene thumperTemp;
    private boolean[][] fealdFree = new boolean[cols][rows];
    private ArrayList<TowerUnit> towerUnitList = new ArrayList<>();
    private ArrayList<Pair<Float, Float>> thumperList = new ArrayList<>();

    //For the worm
    private boolean wormCanBeActivated = false;
    private boolean wormIsActivated = false;
    private Worm worm;

    //For the enemies/waves
    private Wave wave ;
    private long waveDelay = 30;
    private int waveCounter = 0;
    private float spawnrate = 35;
    private Queue<EnemyUnit> enemyWaveQueue = new Queue<>();
    private boolean waveIsRunning = false;
    private ArrayList<EnemyUnit> enemyList = new ArrayList<>();
    //private Harvester harvester;
    //private BossUnit boss;
    //private Infantry infantry;

    //For the bullets
    private ArrayList<Bullet> bullets = new ArrayList<>();
    private Bullet newBullet;
    private long bulletDelay = 30;
    private long bulletDelay2 = 20;

    //Every sounds and background music
    private Mp3.Sound sound;
    private Mp3.Sound thud;
    private Mp3.Music music;
    private Wav.Sound coin;

    public GameScreen(DuneTD parent) {
        this.parent = parent;
    }

    /**
     * Called when this screen becomes the current screen for a {@link Game}.
     * @author Dennis Jehle
     */
    @Override
    public void show() {

        // SpaiR/imgui-java
        ImGui.createContext();
        windowHandle = ((Lwjgl3Graphics) Gdx.graphics).getWindow().getWindowHandle();
        imGuiGlfw.init(windowHandle, true);
        imGuiGl3.init("#version 120");

        // GDX GLTF - Scene Manager
        sceneManager = new SceneManager(64);

        // GDX GLTF - Light
        light = new DirectionalLightEx();
        light.direction.set(1, -3, 1).nor();
        light.color.set(Color.WHITE);
        sceneManager.environment.add(light);

        // GDX GLTF - Image Based Lighting
        IBLBuilder iblBuilder = IBLBuilder.createOutdoor(light);
        environmentCubemap = iblBuilder.buildEnvMap(1024);
        diffuseCubemap = iblBuilder.buildIrradianceMap(256);
        specularCubemap = iblBuilder.buildRadianceMap(10);
        iblBuilder.dispose();

        // GDX GLTF - This texture is provided by the library, no need to have it in your assets.
        brdfLUT = new Texture(Gdx.files.classpath("net/mgsx/gltf/shaders/brdfLUT.png"));

        // GDX GLTF - Cubemaps
        sceneManager.setAmbientLight(1f);
        sceneManager.environment.set(new PBRTextureAttribute(PBRTextureAttribute.BRDFLUTTexture, brdfLUT));
        sceneManager.environment.set(PBRCubemapAttribute.createSpecularEnv(specularCubemap));
        sceneManager.environment.set(PBRCubemapAttribute.createDiffuseEnv(diffuseCubemap));

        // GDX GLTF - Skybox
        skybox = new SceneSkybox(environmentCubemap);
        sceneManager.setSkyBox(skybox);

        // Camera
        camera = new PerspectiveCamera();
        camera.position.set(10.0f, 10.0f, 10.0f);
        camera.lookAt(new Vector3(0, 0, 0));
        sceneManager.setCamera(camera);

        // Camera Input Controller
        cameraInputController = new CameraInputController(camera);

        // Set Input Processor
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(cameraInputController);
        // TODO: add further input processors if needed
        Gdx.input.setInputProcessor(inputMultiplexer);

        // Load all 3D models listed in kenney_assets.txt file in blocking mode
        FileHandle assetsHandle = Gdx.files.internal("kenney_assets.txt");
        String fileContent = assetsHandle.readString();
        kenneyModels = fileContent.split("\\r?\\n");
        for (int i = 0; i < kenneyModels.length; i++) {
            parent.assetManager.load(basePath + kenneyModels[i], SceneAsset.class);
        }
        // Load example enemy models
        parent.assetManager.load("faceted_character/scene.gltf", SceneAsset.class);
        parent.assetManager.load("cute_cyborg/scene.gltf", SceneAsset.class);
        parent.assetManager.load("spaceship_orion/scene.gltf", SceneAsset.class);

        parent.assetManager.load("portal1/scene.gltf", SceneAsset.class);
        parent.assetManager.load("portal2/scene.gltf", SceneAsset.class);
        parent.assetManager.load("thumper/scene.gltf", SceneAsset.class);
        parent.assetManager.load("bullet/scene.gltf", SceneAsset.class);
        parent.assetManager.load("worm/scene.gltf", SceneAsset.class);
        DuneTD.assetManager.finishLoading();

        // Create scene assets for all loaded models
        sceneAssetHashMap = new HashMap<>();
        for (int i = 0; i < kenneyModels.length; i++) {
            SceneAsset sceneAsset = parent.assetManager.get(basePath + kenneyModels[i], SceneAsset.class);
            sceneAssetHashMap.put(kenneyModels[i], sceneAsset);
        }
        SceneAsset bossCharacter = parent.assetManager.get("faceted_character/scene.gltf");
        sceneAssetHashMap.put("faceted_character/scene.gltf", bossCharacter);
        SceneAsset enemyCharacter = parent.assetManager.get("cute_cyborg/scene.gltf");
        sceneAssetHashMap.put("cute_cyborg/scene.gltf", enemyCharacter);
        SceneAsset harvesterCharacter = parent.assetManager.get("spaceship_orion/scene.gltf");
        sceneAssetHashMap.put("spaceship_orion/scene.gltf", harvesterCharacter);

        SceneAsset portal1 = parent.assetManager.get("portal1/scene.gltf");
        sceneAssetHashMap.put("portal1/scene.gltf", portal1);
        SceneAsset portal2 = parent.assetManager.get("portal2/scene.gltf");
        sceneAssetHashMap.put("portal2/scene.gltf", portal2);
        SceneAsset thumper = parent.assetManager.get("thumper/scene.gltf");
        sceneAssetHashMap.put("thumper/scene.gltf", thumper);
        SceneAsset bullet = parent.assetManager.get("bullet/scene.gltf");
        sceneAssetHashMap.put("bullet/scene.gltf", bullet);
        SceneAsset worm = parent.assetManager.get("worm/scene.gltf");
        sceneAssetHashMap.put("worm/scene.gltf", worm);

        createMapExample(sceneManager);

        //load all sounds into game
        sound = (Mp3.Sound) Gdx.audio.newSound(Gdx.files.internal("wormSound/scream.mp3"));
        thud = (Mp3.Sound) Gdx.audio.newSound(Gdx.files.internal("placingSound/thud.mp3"));
        coin = (Wav.Sound) Gdx.audio.newSound(Gdx.files.internal("coin/sound.wav"));
        music = (Mp3.Music) Gdx.audio.newMusic(Gdx.files.internal("backgroundMusic/music.mp3"));

        //activate background music
        music.setVolume(0.2f);
        music.setLooping(true);
        music.play();
    }

    /**
     * Called when the screen should render itself.
     *
     * @author Dennis Jehle
     * @param delta - The time in seconds since the last render.
     */
    @Override
    public void render(float delta) {
        // OpenGL - clear color and depth buffer
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        //bossCharacterAnimationController.update(delta);
        //enemyCharacterAnimationController.update(delta);
        //spaceshipAnimationController.update(delta);

        // SpaiR/imgui-java
        imGuiGlfw.newFrame();
        ImGui.newFrame();

        // GDX GLTF - update scene manager and render scene
        sceneManager.update(delta);
        sceneManager.render();

        modelBatch.begin(camera);
        modelBatch.render(modelList, environment);
        //modelBatch.render(boxInstance);

        //Hud for placing Towers, seeing stats and starting waves
        ImGui.begin("HUD", ImGuiWindowFlags.AlwaysAutoResize);
        ImGui.text("Score: " + score);
        ImGui.text("Spice: " + spice);
        ImGui.text("Health: " + health);
        ImGui.text("Wave:" + waveCounter);
        ImGui.text("Thumper Count: " + thumperCount);
        if (ImGui.button("Buy Bombtower")&& spice >= 100 && !boughtBomb && !boughtSonic && !boughtArtillery && !boughtThumper && !waveIsRunning) {
            long id = coin.play();
            coin.setLooping(id, false);
            spice -= 100;
            boughtBomb = true;
            bombTowerTemp = new Scene(sceneAssetHashMap.get("weapon_cannon.glb").scene);
            sceneManager.addScene(bombTowerTemp);
        }
        if (ImGui.button("Buy Sonictower") && spice >= 70 && !boughtBomb && !boughtSonic && !boughtArtillery && !boughtThumper && !waveIsRunning) {
            long id = coin.play();
            coin.setLooping(id, false);
            spice -= 70;
            boughtSonic = true;
            sonicTowerTemp = new Scene(sceneAssetHashMap.get("towerRound_crystals.glb").scene);
            sceneManager.addScene(sonicTowerTemp);
        }
        if (ImGui.button("Buy Artilleryower")&& spice >= 50 && !boughtBomb && !boughtSonic && !boughtArtillery && !boughtThumper && !waveIsRunning) {
            long id = coin.play();
            coin.setLooping(id, false);
            spice -= 50;
            boughtArtillery = true;
            artilleryTowerTemp = new Scene(sceneAssetHashMap.get("weapon_blaster.glb").scene);
            sceneManager.addScene(artilleryTowerTemp);
        }
        if (ImGui.button("Buy Thumper")&& spice >= 500 && !boughtBomb && !boughtSonic && !boughtArtillery && !boughtThumper && thumperCount < 2 ) {
            long id = coin.play();
            coin.setLooping(id, false);
            spice -= 500;
            boughtThumper = true;
            thumperTemp = new Scene(sceneAssetHashMap.get("thumper/scene.gltf").scene);
            sceneManager.addScene(thumperTemp);
        }

        if (ImGui.button("Activate Worm") && wormCanBeActivated){
            ArrayList<Thumper> thumpers = new ArrayList<>();
            for (TowerUnit tu : towerUnitList){
                if (tu instanceof Thumper) thumpers.add((Thumper) tu);
            }
            worm = new Worm(new Scene(sceneAssetHashMap.get("worm/scene.gltf").scene), thumpers.get(0), thumpers.get(1), cols);
            wormIsActivated = true;
            wormCanBeActivated = false;
            for (Thumper thp : thumpers){
                towerUnitList.remove(thp);
                thumperList.remove(thp.position);
                sceneManager.removeScene(thp.getScene());
            }
            sceneManager.addScene(worm.getScene());
            thumperCount = 0;
            long id = sound.play(0.1f);
            sound.setLooping(id, false);
        }

        //Buttons for spawning enemies. Undo // on the attributes harvester, boss and infantry so the buttons work again
        /*
        if (ImGui.button("Print Enemies")) enemyList.forEach(System.out::println);
        if (ImGui.button("MoveTest Harvester")){
            harvester = new Harvester(new Scene(sceneAssetHashMap.get("spaceship_orion/scene.gltf").scene), getGraphPath(fealdFree), new Tile(0, 0));
            enemyList.add(harvester);
            sceneManager.addScene(harvester.getScene());
        }
        if (ImGui.button("MoveTest Boss")){
            boss = new BossUnit(new Scene(sceneAssetHashMap.get("faceted_character/scene.gltf").scene), getGraphPath(fealdFree), new Tile(0, 0));
            enemyList.add(boss);
            sceneManager.addScene(boss.getScene());
        }
        if (ImGui.button("MoveTest Infantry")){
            infantry = new Infantry(new Scene(sceneAssetHashMap.get("cute_cyborg/scene.gltf").scene), getGraphPath(fealdFree), new Tile(0, 0));
            enemyList.add(infantry);
            sceneManager.addScene(infantry.getScene());
        }

         */

        if (ImGui.button("Start Wave") && !waveIsRunning){
            waveIsRunning = true;
            wave = new Wave(getGraphPath(fealdFree), ++waveCounter, sceneAssetHashMap);
            wave.getWave().forEach($ -> enemyWaveQueue.addLast($));
        }

        delayEnemies();
        if (boughtBomb) placeTower(bombTowerTemp);
        if (boughtSonic) placeTower(sonicTowerTemp);
        if (boughtArtillery) placeTower(artilleryTowerTemp);
        if (boughtThumper) placeThumper(thumperTemp);
        if (!enemyList.isEmpty()) movePath();
        if (wormIsActivated) destroy();
        getBullets();
        ImGui.end();

        outlineGrid();

        modelBatch.render(coloredGridInstance);
        modelBatch.end();

        ImGui.begin("Performance", ImGuiWindowFlags.AlwaysAutoResize);
        ImGui.text(String.format(Locale.US,"deltaTime: %1.6f", delta));
        ImGui.end();

        ImGui.begin("Menu", ImGuiWindowFlags.AlwaysAutoResize);
        if (ImGui.button("Back to menu") || health <=0) {
            music.stop();
            health = 1000;
            spice = 5000;
            parent.changeScreen(ScreenEnum.MENU);
        }
        ImGui.end();

        // SpaiR/imgui-java
        ImGui.render();
        imGuiGl3.renderDrawData(ImGui.getDrawData());
    }



    /**
     * Called when wormIsActivated is true in the render methode
     * Starts the worm and removes every unit which touches the worm
     */
    private void destroy() {
        worm.start();
        ArrayList<TowerUnit> removeTowers = new ArrayList<>();
        for (TowerUnit tu : towerUnitList){
            if (tu.position.getFirst() == Math.floor(worm.getPosition().getFirst()) && tu.position.getSecond() == Math.floor(worm.getPosition().getSecond())){
                removeTowers.add(tu);
            }
        }
        for (EnemyUnit eu : enemyList){
            if (Math.floor(eu.position.getFirst()) == Math.floor(worm.getPosition().getFirst()) && Math.floor(eu.position.getSecond()) == Math.floor(worm.getPosition().getSecond())){
                eu.health = 0;
                spice += eu.getDamage();
            }
        }
        for (TowerUnit tu : removeTowers){
            towerUnitList.remove(tu);
            sceneManager.removeScene(tu.getScene());
            fealdFree[tu.position.getFirst().intValue()][tu.position.getSecond().intValue()] = false;
            spice += tu.price/2;
        }

        if (worm.isReached()){
            sceneManager.removeScene(worm.getScene());
            wormIsActivated = false;
        }
    }

    /**
     * Gets called in the render methode
     * Controls the rate at which speed the enemies get spawned
     */
    private void delayEnemies() {
        if (spawnrate % 10 == 0) spawnrate *= 0.8;
        if (waveDelay >= spawnrate && !enemyWaveQueue.isEmpty()){
            sceneManager.addScene(enemyWaveQueue.first().getScene());
            enemyList.add(enemyWaveQueue.removeFirst());
            waveDelay = 0;
        }
        if (enemyWaveQueue.isEmpty()) waveIsRunning = false;
        waveDelay++;
    }

    /**
     * Gets called in the render methode
     * Checks if an enemy is in range of a tower. If the tower is a bomb tower or an artillery tower then a new bullet instance is created.
     * If the tower is a sonic tower and the enemy isn't slowed yet, than the enemy gets slowed. If the enemy isn't in range of the sonic
     * tower than he gets his original speed
     */
    private void getBullets() {
        bulletDelay += 1;
        bulletDelay2 += 1;
        for (TowerUnit tu : towerUnitList){
            for (EnemyUnit eu : enemyList){
                if (tu.circle.contains(eu.getPosition().getFirst(), eu.getPosition().getSecond())){
                    if (tu instanceof BombTower){
                        if (bulletDelay >= 1000 || !tu.isShooting){
                            newBullet = new Bullet(new Pair<>(tu.position.getFirst(), tu.position.getSecond()), new Scene(sceneAssetHashMap.get("bullet/scene.gltf").scene), tu, eu);
                            bullets.add(newBullet);
                            sceneManager.addScene(newBullet.getScene());
                            tu.isShooting = true;
                            if (bulletDelay >= 1000) bulletDelay = 0;
                        }
                    }else if (tu instanceof ArtilleryTower){
                        if (bulletDelay2 >= 3000 || !tu.isShooting){
                            newBullet = new Bullet(new Pair<>(tu.position.getFirst(), tu.position.getSecond()), new Scene(sceneAssetHashMap.get("bullet/scene.gltf").scene), tu, eu);
                            bullets.add(newBullet);
                            sceneManager.addScene(newBullet.getScene());
                            tu.isShooting = true;
                            if (bulletDelay2 >= 3000) bulletDelay2 = 0;
                        }
                    }else if (tu instanceof SonicTower){
                        if (eu.isGrounded() && !eu.isSlowed){
                            eu.velocity /= 3;
                            eu.isSlowed = true;
                        }
                    }
                }
            }
        }
        boolean shouldBeSlowed;
        for (EnemyUnit eu : enemyList) {
            if (!eu.isSlowed) continue;
            shouldBeSlowed = false;
            for (TowerUnit tu : towerUnitList) {
                if (eu.isSlowed && tu instanceof SonicTower && tu.circle.contains(eu.getPosition().getFirst(), eu.getPosition().getSecond())){
                    shouldBeSlowed = true;
                }
            }
            if (!shouldBeSlowed) {
                eu.velocity *= 3;
                eu.isSlowed = false;
            }
        }
        shootBullets();
    }

    /**
     * Gets called in the getBullets() methode
     * If there where bullets initialized in getBullets(), then the bullet position will get updated by the methode update() from the Bullet class.
     * Also calculates the aoe damage of the bomb tower if the bullet was shot from a bomb tower.
     * If the bullet reached his target than it will also be removed by this methode.
     */
    private void shootBullets() {
        ArrayList<Bullet> removeBullet = new ArrayList<>();
        for (Bullet bullet : bullets){
            bullet.update();
            if (bullet.remove) {
                if (bullet.isBomb) {
                    Circle bomb = new Circle(bullet.getPosition().getFirst(), bullet.getPosition().getSecond(), 1);
                    enemyList.forEach($ -> {
                        if (bomb.contains($.getPosition().getFirst(), $.getPosition().getSecond())) $.health -= bullet.getFromTower().damage;
                    });
                }
                sceneManager.removeScene(bullet.getScene());
                bullet.getFromTower().isShooting = false;
                removeBullet.add(bullet);
            }
        }
        for (Bullet bullet : removeBullet){
            bullets.remove(bullet);
        }
    }

    /**
     * Gets called when the player could successfully buy a thumper (boughtThumper is true).
     * The model of the thumper gets dragged with the mouse until the player ether places the model on a valid grid with left click
     * or cancels the placement with right click.
     * @param scene is the scene of the tower that the player wants to place
     */
    private void placeThumper(Scene scene){
        mouseRay = camera.getPickRay(Gdx.input.getX(), Gdx.input.getY());
        float distance = (-mouseRay.origin.y / mouseRay.direction.y);
        if (boughtThumper && thumperCount < 2){
            mouseVector.set(mouseRay.direction).scl(distance).add(mouseRay.origin);
            scene.modelInstance.transform.setToTranslation(mouseVector).scale(0.0125f,0.0125f,0.0125f);
            for (int i = 0; i < modelList.size(); i++){
                float tempX = boundingBoxList.get(i).getCenterX();
                float tempZ = boundingBoxList.get(i).getCenterZ();
                if(Intersector.intersectRayBounds(mouseRay, boundingBoxList.get(i), mouseVector) && Gdx.input.isButtonJustPressed(MouseEvent.NOBUTTON) && fieldFree(tempX, tempZ) && noThumper(tempX, tempZ) && validateThumper(tempX,tempZ)){
                    thumperCount++;
                    thumperList.add(new Pair<>(tempX, tempZ));
                    towerUnitList.add(new Thumper(new Pair<>(tempX, tempZ), scene));
                    scene.modelInstance.transform.setToTranslation(tempX, 0.1f, tempZ).scale(0.0125f,0.0125f,0.0125f);
                    if (thumperCount == 2) wormCanBeActivated  =true;
                    boughtSonic = false;
                    boughtThumper = false;
                    boughtArtillery = false;
                    boughtBomb = false;
                    break;
                }
                if(Gdx.input.isButtonJustPressed(MouseEvent.BUTTON1)) {
                    if (boughtThumper) spice += 500;
                    boughtSonic = false;
                    boughtThumper = false;
                    boughtArtillery = false;
                    boughtBomb = false;
                    sceneManager.removeScene(scene);
                    break;
                }
            }
        }
    }

    /**
     * Gets called when the player could successfully buy a bomb tower, sonic tower or artillery tower (boughtBomb, boughtSonic or boughtArtillery is true).
     * The model of the tower gets dragged with the mouse until the player ether places the model on a valid grid with left click
     * or cancels the placement with right click.
     * @param scene is the scene of the tower that the player wants to place
     */
    private void placeTower(Scene scene) {
        mouseRay = camera.getPickRay(Gdx.input.getX(), Gdx.input.getY());
        float distance = (-mouseRay.origin.y / mouseRay.direction.y);
        if (boughtSonic ||boughtArtillery|| boughtBomb){
            mouseVector.set(mouseRay.direction).scl(distance).add(mouseRay.origin);
            scene.modelInstance.transform.setTranslation(mouseVector);
            for (int i = 0; i < modelList.size(); i++){
                float tempX = boundingBoxList.get(i).getCenterX();
                float tempZ = boundingBoxList.get(i).getCenterZ();
                if(Intersector.intersectRayBounds(mouseRay, boundingBoxList.get(i), mouseVector) && Gdx.input.isButtonJustPressed(MouseEvent.NOBUTTON) && fieldFree(tempX, tempZ) && noThumper(tempX, tempZ) && enemyList.isEmpty()){
                    fealdFree[(int) tempX][(int) tempZ] = true;
                    if (getGraphPath(fealdFree).getCount() != 0){
                        long id = thud.play();
                        thud.setLooping(id, false);
                        scene.modelInstance.transform.setToTranslation(tempX, 0.1f, tempZ);
                        if (boughtSonic) towerUnitList.add(new SonicTower(new Pair<>(tempX, tempZ), scene));
                        if (boughtArtillery) towerUnitList.add(new ArtilleryTower(new Pair<>(tempX, tempZ), scene));
                        if (boughtBomb) towerUnitList.add(new BombTower(new Pair<>(tempX, tempZ), scene));
                        boughtSonic = false;
                        boughtThumper = false;
                        boughtArtillery = false;
                        boughtBomb = false;
                        break;
                    } else {
                        fealdFree[(int) tempX][(int) tempZ] = false;
                    }
                }
                if(Gdx.input.isButtonJustPressed(MouseEvent.BUTTON1)) {
                    if (boughtArtillery) spice += 50;
                    if (boughtSonic) spice += 70;
                    if (boughtBomb) spice += 100;
                    sceneManager.removeScene(scene);
                    boughtSonic = false;
                    boughtThumper = false;
                    boughtArtillery = false;
                    boughtBomb = false;
                    break;
                }
            }
        }

    }

    /**
     * Gets called in placeTower() for checking if there is a thumper on the tile
     * @param tempX x coordinate of the tile
     * @param tempZ z coordinate of the tile
     * @return true if there is no thumper on the tile
     */
    private boolean noThumper(float tempX, float tempZ){
        for (Pair<Float, Float> thumper : thumperList) {
            if (thumper.getFirst() == tempX && thumper.getSecond() == tempZ) return false;
        }
        return true;
    }

    /**
     * Gets called in placeTower() and placeThumper().
     * Checks if there is already a tower on the current tile or the position of the tile is this of a portal
     * @param centerX x coordinate of the tile
     * @param centerZ z coordinate of the tile
     * @return true if the tile is free and the tile is not the position of the portals
     */
    private boolean fieldFree(float centerX, float centerZ) {
        if (centerX == 0 && centerZ == 0 || centerX == cols-1 && centerZ == rows-1) return false;
        return !fealdFree[(int) centerX][(int) centerZ];
    }

    /**
     * Gets called in placeThumper().
     * Validates the placement of the second thumper which must be in the same row or column of the first
     * @param tempX X position of tile
     * @param tempZ Z position of tile
     * @return true if there is no thumper placed yet or the second thumper is in the row/column of the first one
     */
    private boolean validateThumper(float tempX, float tempZ) {
        if (thumperCount == 0) return true;
        for (TowerUnit tower : towerUnitList) {
            if (tower instanceof Thumper){
                if ((tower.position.getFirst() == tempX || tower.position.getSecond() == tempZ) && thumperCount <=2) return true;
            }
        }
        return false;
    }

    /**
     * Gets called whenever a new object of EnemyUnit gets initialized
     * Creates a new Tile for every false value in current fealdFree with the corresponding position and connects every adjacent Tiles of every tile instance.
     * @return the shortest path from the start portal to the goal portal with the a* algorithm from the libgdx ai library.
     */
    public GraphPath<Tile> getGraphPath(boolean[][] currentFeald){
        DuneGraph dg = new DuneGraph();
        Tile startTile = new Tile(0, 0);
        Tile goalTile = new Tile(cols-1, rows-1);
        dg.addTile(startTile);
        for (int i = 0; i < currentFeald.length; i++){
            for (int j = 0; j < currentFeald[i].length; j++){
                if (!currentFeald[i][j]) {
                    dg.addTile(new Tile(i, j));
                }
            }
        }
        dg.addTile(goalTile);
        for (int i = 0; i < dg.tiles.size; i++){
            for (int j = 0; j < dg.tiles.size; j++){
                if (dg.tiles.get(i).x == dg.tiles.get(j).x && dg.tiles.get(i).z == dg.tiles.get(j).z+1) dg.connectTiles(dg.tiles.get(i), dg.tiles.get(j));
                if (dg.tiles.get(i).x == dg.tiles.get(j).x && dg.tiles.get(i).z == dg.tiles.get(j).z-1) dg.connectTiles(dg.tiles.get(i), dg.tiles.get(j));
                if (dg.tiles.get(i).z == dg.tiles.get(j).z && dg.tiles.get(i).x == dg.tiles.get(j).x+1) dg.connectTiles(dg.tiles.get(i), dg.tiles.get(j));
                if (dg.tiles.get(i).z == dg.tiles.get(j).z && dg.tiles.get(i).x == dg.tiles.get(j).x-1) dg.connectTiles(dg.tiles.get(i), dg.tiles.get(j));
            }
        }
        return dg.findPath(startTile, goalTile);
    }

    /**
     * Gets called in render() whenever the enemyList isn't empty
     * Moves every enemy by the corresponding velocity and checks if the enemy should be removed. Factors are did the enemy
     * reach the goalPortal or is the health of the enemy zero
     */
    private void movePath(){
        ArrayList<EnemyUnit> removeEnemies = new ArrayList<>();
        for (EnemyUnit eu: enemyList) {
            eu.step();
            if (eu.reached || eu.health <= 0){
                if (eu.health <= 0) {
                    spice += eu.damage;
                    score += eu.damage * 10;
                }
                sceneManager.removeScene(eu.getScene());
                removeEnemies.add(eu);
            }
        }
        for (EnemyUnit eu : removeEnemies) {
            enemyList.removeIf($ -> $.equals(eu));
        }

    }

    /**
     * Gets called in the EnemyUnit class if an enemy reached the goalPortal
     * Decrements the players health by the damage of an EnemyUnit
     * @param damage the damage of an EnemyUnit
     */
    public static void setHealth(long damage){
        health -= damage;
    }

    /**
     * Gets called in render()
     * Outlines the grid where the mouse is currently hovering over
     */
    private void outlineGrid(){
        mouseRay = camera.getPickRay(Gdx.input.getX(), Gdx.input.getY());
        coloredGrid = modelBuilder.createLineGrid(1, 1, modelDimensions.x, modelDimensions.z,
                new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);

        for (int i = 0; i < modelList.size(); i++){
            if(Intersector.intersectRayBounds(mouseRay, boundingBoxList.get(i), mouseVector)){
                coloredGridInstance = new ModelInstance(coloredGrid, boundingBoxList.get(i).getCenterX(), 0.11f, boundingBoxList.get(i).getCenterZ());
            }
        }
    }

    public static long getHealth() {
        return health;
    }

    public ArrayList<BoundingBox> getBoundingBoxList() {
        return boundingBoxList;
    }

    public ArrayList<ModelInstance> getModelList() {
        return modelList;
    }

    public ArrayList<Pair<Float, Float>> getThumperList() {
        return thumperList;
    }

    public boolean[][] getFealdFree() {
        return fealdFree;
    }

    public HashMap<String, SceneAsset> getSceneAssetHashMap() {
        return sceneAssetHashMap;
    }

    public float getSpawnrate() {
        return spawnrate;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getThumperCount() {
        return thumperCount;
    }

    public int getWaveCounter() {
        return waveCounter;
    }

    public long getScore() {
        return score;
    }

    public long getSpice() {
        return spice;
    }

    public long getWaveDelay() {
        return waveDelay;
    }

    public Queue<EnemyUnit> getEnemyWaveQueue() {
        return enemyWaveQueue;
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public Wave getCurrentWave() {
        return wave;
    }

    public Worm getWorm() {
        return worm;
    }

    public ArrayList<EnemyUnit> getEnemyList() {
        return enemyList;
    }

    public void addEnemy(EnemyUnit eu){
        enemyList.add(eu);
    }

    public ArrayList<TowerUnit> getTowerUnitList() {
        return towerUnitList;
    }

    public void addTower(TowerUnit tu){
        towerUnitList.add(tu);
    }

    public void setFealdFree(boolean[][] newFeald){
        this.fealdFree = newFeald;
    }



    @Override
    public void resize(int width, int height) {
        // GDX GLTF - update the viewport
        sceneManager.updateViewport(width, height);
    }

    @Override
    public void pause() {
        // TODO: implement pause logic if needed
    }

    @Override
    public void resume() {
        // TODO: implement resume logic if needed
    }

    @Override
    public void hide() {
        // TODO: implement hide logic if needed
    }

    /**
     * This function acts as a starting point.
     * It generate a simple rectangular map with towers placed on it.
     * It doesn't provide any functionality, but it uses some common ModelInstance specific functions.
     * Feel free to modify the values and check the results.
     *
     * Added start and end portal
     *
     * @param sceneManager
     */
    private void createMapExample(SceneManager sceneManager) {

        Vector3 groundTileDimensions = new Vector3();
        modelBatch = new ModelBatch();
        modelBuilder = new ModelBuilder();

        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.5f, 0.5f, 0.5f, 1));

        // Simple way to generate the example map
        for (int i = 0; i < rows; i++) {
            for (int k = 0; k < cols; k++) {
                // Create a new Scene object from the tile_dirt gltf model
                Scene gridTile = new Scene(sceneAssetHashMap.get("tile_dirt.glb").scene);
                // Create a new BoundingBox, this is useful to check collisions or to get the model dimensions
                BoundingBox boundingBoxField = new BoundingBox();
                // Calculate the BoundingBox from the given ModelInstance
                gridTile.modelInstance.calculateBoundingBox(boundingBoxField);
                // Create Vector3 to store the ModelInstance dimensions

                // Read the ModelInstance BoundingBox dimensions
                boundingBoxField.getDimensions(modelDimensions);
                // TODO: refactor this if needed, e.g. if ground tiles are not all the same size
                groundTileDimensions.set(modelDimensions);
                // Set the ModelInstance to the respective row and cell of the map
                gridTile.modelInstance.transform.setToTranslation(k * modelDimensions.x, 0.0f, i * modelDimensions.z);
                // Add the Scene object to the SceneManager for rendering
                sceneManager.addScene(gridTile);

                grid = modelBuilder.createLineGrid(1, 1, modelDimensions.x, modelDimensions.z,
                        new Material(ColorAttribute.createDiffuse(Color.BLACK)),
                        VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
                gridInstance = new ModelInstance(grid, k * modelDimensions.x, 0.1f, i * modelDimensions.z);
                modelList.add(gridInstance);

                boundingBoxList.add(boundingBoxField.mul(gridInstance.transform));
                // it could be useful to store the Scene object reference outside this method
            }
        }
        //place endPortal
        Scene portal1 = new Scene(sceneAssetHashMap.get("portal1/scene.gltf").scene);
        portal1.modelInstance.transform.setToTranslation(cols-1, groundTileDimensions.y+0.05f, rows-1 + 0.3f)
                .scale(0.25f, 0.25f, 0.25f)
                .rotate(new Vector3(1.0f, 0.0f, 1.0f), 0.0f);
        sceneManager.addScene(portal1);

        //place startPortal
        Scene portal2 = new Scene(sceneAssetHashMap.get("portal2/scene.gltf").scene);
        portal2.modelInstance.transform.setToTranslation(0.0f, groundTileDimensions.y+0.05f, 0.0f)
                .scale( 0.0004f,0.0004f,0.0004f);
        sceneManager.addScene(portal2);

        /*
        // place boss character
        Scene bossCharacter = new Scene(sceneAssetHashMap.get("faceted_character/scene.gltf").scene);
        bossCharacter.modelInstance.transform.setToTranslation(0.0f, groundTileDimensions.y, 2.0f).scale(0.005f, 0.005f, 0.005f);
        sceneManager.addScene(bossCharacter);

        bossCharacterAnimationController = new AnimationController(bossCharacter.modelInstance);
        bossCharacterAnimationController.setAnimation("Armature|Run", -1);


        // place enemy character
        Scene enemyCharacter = new Scene(sceneAssetHashMap.get("cute_cyborg/scene.gltf").scene);
        enemyCharacter.modelInstance.transform.setToTranslation(1.0f, groundTileDimensions.y, 2.0f)
                .scale(0.02f, 0.04f, 0.03f).rotate(new Vector3(0.0f, 1.0f, 0.0f), 180.0f);
        sceneManager.addScene(enemyCharacter);

        enemyCharacterAnimationController = new AnimationController(enemyCharacter.modelInstance);
        enemyCharacterAnimationController.setAnimation("RUN", -1);

        // place spaceship character
        Scene spaceshipCharacter = new Scene(sceneAssetHashMap.get("spaceship_orion/scene.gltf").scene);
        spaceshipCharacter.modelInstance.transform.setToTranslation(2.0f, 0.25f, 2.0f)
                .scale(0.2f, 0.2f, 0.2f);
        sceneManager.addScene(spaceshipCharacter);

        spaceshipAnimationController = new AnimationController(spaceshipCharacter.modelInstance);
        spaceshipAnimationController.setAnimation("Action", -1);

         */
    }

    /**
     * Disposes everything that is in the methode when the screen gets closed/switched
     */
    @Override
    public void dispose() {
        // GDX GLTF - dispose resources
        sceneManager.dispose();
        environmentCubemap.dispose();
        diffuseCubemap.dispose();
        specularCubemap.dispose();
        brdfLUT.dispose();
        skybox.dispose();
        sound.dispose();
        music.dispose();
    }

}
